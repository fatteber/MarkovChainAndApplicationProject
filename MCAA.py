# =========================================================
# Old version of the MCAA algorithm
# Does not contains the optimization of the algorithm
# =========================================================

import numpy as np
import random
import math

class Chain:
    def __init__(self, m, d, s, a, beta, resetProbability = 0.05, round = 100_000) -> None:
        self.s = s
        self.m = m
        self.d = d
        self.a = a
        self.beta = beta
        self.resetProbability = resetProbability
        self.round = round

    def getEpsilon(self) -> np.ndarray:
        return np.random.normal(0, self.a, self.m)
    
    def getX(self) -> np.ndarray:
        return np.random.normal(0, 1, (self.m, self.d))
    
    def accept(self, X: np.ndarray, y: np.ndarray, i: np.ndarray, j: np.ndarray) -> bool:
        return random.random() < self.acceptProbability(X, y, i, j)
    
    def smooth(self, X: np.ndarray) -> np.ndarray:
        return X
    
    def loop(self):
        # set numpy random seed
        # np.random.seed(0)

        state = self.getInitial()

        self.epsilon = self.getEpsilon()
        self.theta = self.getTheta()
        X = self.getX()
        y = self.smooth(X @ self.theta + self.epsilon)

        minv = 100000
        mina = 0

        for i in range(self.round):
            new_state = self.next(state)
            if self.accept(X, y, state, new_state):
                state = new_state
            if np.linalg.norm(self.smooth(X @ state) - y) < minv:
                minv = np.linalg.norm(self.smooth(X @ state) - y)
                mina = state
            if minv <= 0.0:
                print(i,"steps")
                break

            # Jump back to the minima
            # From what we can see, it speed up the convergence
            if random.random() < self.resetProbability:
                state = mina

        print(">>", minv)

        return state

class ChainQ1(Chain):
    def getInitial(self) -> np.ndarray:
        return np.zeros(self.d)
    
    def getTheta(self) -> np.ndarray:
        return np.random.choice([0, 1], self.d)

    def next(self, x: np.ndarray) -> np.ndarray:
        y = x.copy()
        # choise a random index
        i = np.random.randint(0, self.d)
        # swap the value
        y[i] = 1 - y[i]
        return y
    
    def acceptProbability(self, X: np.ndarray, y: np.ndarray, i: np.ndarray, j: np.ndarray) -> float:
        bottom = math.exp(-self.beta * np.linalg.norm(X @ i - y) / self.m)
        top = math.exp(-self.beta * np.linalg.norm(X @ j - y) / self.m)

        return min(1, top / bottom)
    
class ChainQ2(Chain):
    def getInitial(self) -> np.ndarray:
        y = np.zeros(self.d)
        y[:self.s] = 1
        return y
    
    def getTheta(self) -> np.ndarray:
        # return a random vector with s 1 and d - s 0
        theta = np.zeros(self.d)
        theta[:self.s] = 1
        np.random.shuffle(theta)
        return theta

    def next(self, x: np.ndarray) -> np.ndarray:
        y = x.copy()
        # choice a random index where the value is 1
        i = np.random.choice(np.where(x == 1)[0])
        # choice a random index where the value is 0
        j = np.random.choice(np.where(x == 0)[0])

        # swap the value
        y[i] = 0
        y[j] = 1

        return y
    
    def acceptProbability(self, X: np.ndarray, y: np.ndarray, i: np.ndarray, j: np.ndarray) -> float:
        bottom = math.exp(-self.beta * np.linalg.norm(X @ i - y) / self.m)
        top = math.exp(-self.beta * np.linalg.norm(X @ j - y) / self.m)

        return min(1, top / bottom)
    

class ChainQ3(ChainQ2):
    def acceptProbability(self, X: np.ndarray, y: np.ndarray, i: np.ndarray, j: np.ndarray) -> float:
        bottom = math.exp(-self.beta * np.linalg.norm(np.sign(X @ i - y)) / self.m)
        top = math.exp(-self.beta * np.linalg.norm(np.sign(X @ j - y)) / self.m)

        return min(1, top / bottom)
    
    def smooth(self, X: np.ndarray) -> np.ndarray:
        return np.sign(X)
    
def plot(values: np.ndarray, title: str):
    import matplotlib.pyplot as plt
    plt.plot(values)
    plt.title(title)
    # save the figure
    plt.savefig(title + ".png")
    
if __name__ == "__main__":
    # values = []
    # for i in range(1, 100, 10):
    #     currents = []
    #     for b in range(1, 20):
    #         chain = ChainQ1(m = i, d = 100, s = 0, a = 0, beta = 6 / 10, resetProbability = 0.05, round=100_000)
    #         currents.append(np.linalg.norm(chain.loop() - chain.theta) / chain.d)
    #     values.append(np.mean(currents))
    # plot(values, "Q1")

    chain = ChainQ1(m = 100, d = 100, s = 10, a = 0, beta = 6 / 10, resetProbability = 0.05, round=100_000)
    chain.loop()

    chain = ChainQ2(m = 100, d = 100, s = 10, a = 0, beta = 6 / 10, resetProbability = 0.05, round=100_000)
    chain.loop()

    chain = ChainQ3(m = 100, d = 100, s = 10, a = 0, beta = 6 / 10, resetProbability = 0.05, round=100_000)
    chain.loop()