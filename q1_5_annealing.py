import numpy as np
import random
import math
import matplotlib.pyplot as plt
import time

class Chain:
    def __init__(self, m, d, s, a, beta, resetProbability = 0.05, round = 100_000) -> None:
        self.s = s
        self.m = m
        self.d = d
        self.a = a
        self.beta = beta
        self.resetProbability = resetProbability
        self.round = round

    def getEpsilon(self) -> np.ndarray:
        return np.random.normal(0, self.a, self.m)
    
    def getX(self) -> np.ndarray:
        return np.random.normal(0, 1, (self.m, self.d))
    
    def accept(self, X: np.ndarray, y: np.ndarray, i: np.ndarray, j: np.ndarray, k: int) -> bool:
        return random.random() < self.acceptProbability(X, y, i, j, k)
    
    def loop(self):
        state = self.getInitial()

        epsilon = self.getEpsilon()
        theta = self.getTheta()
        X = self.getX()
        y = X @ theta + epsilon

        self.XX = X.T @ X
        self.yX = y.T @ X

        minv = 100000
        mina = state
        for k in range(0,10):
            for i in range(self.round//10):
                (new_state, k) = self.next(state)
                if self.accept(X, y, state, new_state, k):
                    state = new_state
                    if self.resetProbability > 0 and np.linalg.norm(X @ state - y) < minv:
                        minv = np.linalg.norm(X @ state - y)
                        mina = state
                        if minv <= 0.0:
                            return i
            self.beta *= 10

            # Jump back to the minima
            # From what we can see, it speed up the convergence
            if random.random() < self.resetProbability:
                state = mina

        print(">>", minv)
        if self.resetProbability > 0:
            state = mina

        return self.round

class ChainQ1(Chain):
    def getInitial(self) -> np.ndarray:
        return np.zeros(self.d)
    
    def getTheta(self) -> np.ndarray:
        return np.random.choice([0, 1], self.d)

    def next(self, x: np.ndarray) -> (np.ndarray, int):
        y = x.copy()
        # choise a random index
        i = np.random.randint(0, self.d)
        # swap the value
        y[i] = 1 - y[i]
        return (y, i)
    
    def acceptProbability(self, X: np.ndarray, y: np.ndarray, i: np.ndarray, j: np.ndarray, k: int) -> float:
        s = (j -i)[k]

        h = np.exp(-self.beta * (2*(i.T.dot(self.XX[:, k])*s) - 2*(self.yX[k]*s) + self.XX[k, k]))

        return min(1, h)
    
if __name__ == "__main__":
    values = []
    times = []
    d = 2000
    m = 2000
    bs = (0.001, 0.01, 0.1, 1, 10, 100, 1000)
    logbs = np.log(bs)
    for b in bs:
        sub = []
        for i in range(10):
            start = time.time()
            chain = ChainQ1(m = m, d = d, s = 0, a = 0, beta = b, resetProbability = 0.01, round=d * 100)
            sub.append(chain.loop())
            end = time.time()

            times.append(end - start)

            print("Time:", end - start)
        values.append(sum(sub) / len(sub))
    # Draw the graph
    plt.plot(logbs, values)
    # save the graph
    plt.savefig("graph_q_15_annealing.png")