import numpy as np
import random
import math
import matplotlib.pyplot as plt
import time

class Chain:
    def __init__(self, m, d, s, a, beta, resetProbability = 0.05, round = 100_000) -> None:
        self.s = s
        self.m = m
        self.d = d
        self.a = a
        self.beta = beta
        self.resetProbability = resetProbability
        self.round = round

    def getEpsilon(self) -> np.ndarray:
        return np.random.normal(0, self.a, self.m)
    
    def getX(self) -> np.ndarray:
        return np.random.normal(0, 1, (self.m, self.d))
    
    def accept(self, X: np.ndarray, y: np.ndarray, i: np.ndarray, j: np.ndarray, k: (int, int)) -> bool:
        return random.random() < self.acceptProbability(X, y, i, j, k)
    
    def loop(self):
        state = self.getInitial()

        epsilon = self.getEpsilon()
        theta = self.getTheta()
        X = self.getX()
        y = X @ theta + epsilon

        self.XX = X.T @ X
        self.yX = y.T @ X

        minv = 100000
        mina = state
        for k in range(0,10):
            for i in range(self.round//10):
                (new_state, k) = self.next(state)
                if self.accept(X, y, state, new_state, k):
                    state = new_state
                    if self.resetProbability > 0 and np.linalg.norm(X @ state - y) < minv:
                        minv = np.linalg.norm(X @ state - y)
                        mina = state
                    if minv <= 0.0:
                        print(i,"steps")
                        break
            self.beta *= 10

            # Jump back to the minima
            # From what we can see, it speed up the convergence
            if random.random() < self.resetProbability:
                state = mina

        print(">>", minv)

        return (state, np.linalg.norm(state - theta)**2)

class ChainQ2(Chain):
    def getInitial(self) -> np.ndarray:
        y = np.zeros(self.d)
        y[:self.s] = 1
        return y
    
    def getTheta(self) -> np.ndarray:
        # return a random vector with s 1 and d - s 0
        theta = np.zeros(self.d)
        theta[:self.s] = 1
        np.random.shuffle(theta)
        return theta

    def next(self, x: np.ndarray) -> (np.ndarray, (int,int)):
        y = x.copy()
        # choice a random index where the value is 1
        i = np.random.choice(np.where(x == 1)[0])
        # choice a random index where the value is 0
        j = np.random.choice(np.where(x == 0)[0])

        # swap the value
        y[i] = 0
        y[j] = 1

        return (y, (i, j))
    
    def acceptProbability(self, X: np.ndarray, y: np.ndarray, i: np.ndarray, j: np.ndarray, k: (int,int)) -> float:
        h = np.exp(-self.beta * (2*(i.T.dot(self.XX[:, k[1]] - self.XX[:, k[0]])) - 2*(self.yX[k[1]] - self.yX[k[0]]) + self.XX[k[1], k[1]] + self.XX[k[0], k[0]] - self.XX[k[1], k[0]] - self.XX[k[0], k[1]]))

        return min(1, h)
    
if __name__ == "__main__":
    #chain = ChainQ2(m = 1000, d = 1000, s = 10, a = 1, beta = 0.1, resetProbability = 0.00, round=1000 * 100)
    #chain.loop()
    #exit()
    for d in (2000, 3500, 5000):
        values = []
        for m in range(100, 2000, 100):
            sub = []
            for i in range(1):
                start = time.time()

                chain = ChainQ2(m = m, d = d, s = d//100, a = 1, beta = 0.1, resetProbability = 0.01, round=d * 100)
                sub.append(chain.loop()[1] / (2 * (d//100)))
                end = time.time()

                print("Time:", end - start)
            values.append(sum(sub) / len(sub))
        # Draw the graph
        plt.plot(range(100, 2000, 100), values, label="d = " + str(d))
    plt.legend()
    # save the graph
    plt.savefig("graph_q2.png")