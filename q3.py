import numpy as np
import random
import math
import matplotlib.pyplot as plt
import time

class Chain:
    def __init__(self, m, d, s, a, beta, resetProbability = 0.05, round = 100_000) -> None:
        self.s = s
        self.m = m
        self.d = d
        self.a = a
        self.beta = beta
        self.resetProbability = resetProbability
        self.round = round

    def getEpsilon(self) -> np.ndarray:
        return np.random.normal(0, self.a, self.m)
    
    def getX(self) -> np.ndarray:
        return np.random.normal(0, 1, (self.m, self.d))
    
    def accept(self, X: np.ndarray, y: np.ndarray, i: np.ndarray, j: np.ndarray, k: (int, int)) -> bool:
        return random.random() < self.acceptProbability(X, y, i, j, k)
    
    def loop(self):
        state = self.getInitial()

        epsilon = self.getEpsilon()
        theta = self.getTheta()
        X = self.getX()
        y = np.sign(X @ theta + epsilon)

        self.XX = X.T @ X
        self.yX = y.T @ X

        minv = 100000
        mina = state

        for i in range(self.round):
            (new_state, k) = self.next(state)
            if self.accept(X, y, state, new_state, k):
                state = new_state
                if self.resetProbability > 0 and np.linalg.norm(np.sign(X @ state) - y) < minv:
                    minv = np.linalg.norm(np.sign(X @ state) - y)
                    mina = state
                if minv <= 0.0:
                    print(i,"steps")
                    break

            # Jump back to the minima
            # From what we can see, it speed up the convergence
            if random.random() < self.resetProbability:
                state = mina

        print(">>", minv)

        return (state, np.linalg.norm(state - theta)**2)

class ChainQ3(Chain):
    def getInitial(self) -> np.ndarray:
        y = np.zeros(self.d)
        y[:self.s] = 1
        return y
    
    def getTheta(self) -> np.ndarray:
        # return a random vector with s 1 and d - s 0
        theta = np.zeros(self.d)
        theta[:self.s] = 1
        np.random.shuffle(theta)
        return theta
    
    def next(self, x: np.ndarray) -> (np.ndarray, (int,int)):
        y = x.copy()
        # choice a random index where the value is 1
        i = np.random.choice(np.where(x == 1)[0])
        # choice a random index where the value is 0
        j = np.random.choice(np.where(x == 0)[0])

        # swap the value
        y[i] = 0
        y[j] = 1

        return (y, (i, j))
    
    def acceptProbability(self, X: np.ndarray, y: np.ndarray, i: np.ndarray, j: np.ndarray, k: (int,int)) -> float:
        #h = np.exp(-self.beta * (2*(i.T.dot(self.XX[:, k[1]] - self.XX[:, k[0]])) - 2*(self.yX[k[1]] - self.yX[k[0]]) + self.XX[k[1], k[1]] + self.XX[k[0], k[0]] - self.XX[k[1], k[0]] - self.XX[k[0], k[1]]))
        left = (y - np.ones(self.m))/2
        right_i = 0.5*(np.ones(self.m) + np.array([math.erf(k) for k in (X @ i)/(math.sqrt(2) * self.a)]))
        right_j = 0.5*(np.ones(self.m) + np.array([math.erf(k) for k in (X @ j)/(math.sqrt(2) * self.a)]))

        m1 = -np.sum(np.log(y * right_i - left))
        m2 = -np.sum(np.log(y * right_j - left))

        a = min(1, np.exp(-self.beta * (m2 - m1)))
        
        return a
    
if __name__ == "__main__":
    for d in (500, 1000, 2000):
        values = []
        #d = 500
        for m in range(100, 2000, 50):
            sub = []
            for i in range(1):
                start = time.time()

                chain = ChainQ3(m = m, d = d, s = d//100, a = 1, beta = 1, resetProbability = 0.01, round=d * 100)
                sub.append(chain.loop()[1] / (2 * (d//100)))
                end = time.time()

                print("Time:", end - start)
            values.append(sum(sub) / len(sub))
        # Draw the graph
        plt.plot(range(100, 2000, 50), values, label="d = " + str(d))
        plt.xlabel("m")
        
    # save the graph
    plt.savefig("graph_q3.png")