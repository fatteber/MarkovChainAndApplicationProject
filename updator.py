import os
import time
from datetime import datetime
import subprocess

def git_add_commit_push(directory, commit_message="Auto commit"):
    try:
        # Change working directory to the specified directory
        os.chdir(directory)

        # Add all files to staging area
        subprocess.run(['git', 'add', '.'])

        # Commit changes
        subprocess.run(['git', 'commit', '-m', commit_message])

        # Push changes to the remote repository
        subprocess.run(['git', 'push'])

        print(f"Changes committed and pushed at {datetime.now()}")

    except Exception as e:
        print(f"An error occurred: {str(e)}")

def watch_directory(directory, interval_seconds=60):
    print(f"Watching directory '{directory}' for changes...")

    while True:
        # Get the list of files in the directory
        files = [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]

        # Get the modification time of each file
        file_times = {f: os.path.getmtime(os.path.join(directory, f)) for f in files}

        # Wait for the specified interval
        time.sleep(interval_seconds)

        # Check if any file has been updated
        updated_files = [f for f in files if os.path.getmtime(os.path.join(directory, f)) > file_times[f]]

        if updated_files:
            print(f"Detected changes in the following files: {', '.join(updated_files)}")
            git_add_commit_push(directory)

# Example usage
watch_directory('.', interval_seconds=60)